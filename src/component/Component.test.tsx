import React from 'react';
import { render, screen } from '@testing-library/react';
import { Component } from './Component';

describe('<Component />', () => {
  test('rendered text', () => {
    render(<Component title='title'>Sample Component</Component>);
    expect(screen.getByText('Sample Component')).toBeDefined();
  });
});
